# XLModbusTCP

#### 介绍
新时代的Modbus TCP通信库

该库的特点

1：某一块内存映射成对象

2：可以读写保持寄存器位

#### 使用说明
声明：private IModbus _modbus;

初始化：_modbus = new ModbusTcp(txtIP.Text, Convert.ToInt32(txtPORT.Text), Convert.ToByte(txtADDR.Text));

打开：_modbus.Open();

关闭：_modbus.Close();

注意事项：该库异常直接抛出，请用户自行捕获


#### 方法大全

保持寄存器方法 写值支持类型：（int uint short ushort float double long ulong) 

单写       bool SetRegister(int iaddress, object value);

多写        bool SetRegister<TValue>(int iaddress, List<object> value) where TValue : struct;

写位（慎用，会覆盖其他位）        bool SetBitRegister(int iaddress, int offset, bool value);

写位（建议）        bool SetBitRegister(int iaddress, ushort old_value, int offset, bool value);

多位写        bool SetBitsRegister(int iaddress, List<bool> value);

多写（建议使用泛型的那个方法）        bool SetRegister(int iaddress, ushort[] value);

多读位        List<bool> ReadBitsRegister(int iaddress, int count);

单读位        bool ReadBitRegister(int iaddress, int offset = 0);

单读        TValue ReadRegister<TValue>(int iaddress) where TValue : struct

读多字        List<ushort> ReadRegister(int iaddress, int count);

读类        T ReadClass<T>(int iaddress) where T : class;

输入寄存器方法

单读        ushort ReadInputRegister(int iaddress);

多读        List<ushort> ReadInputRegister(int iaddress, int count);

单读位        bool ReadInputBitRegister(int iaddress, int offset = 0);

多读位        List<bool> ReadInputBitsRegister(int iaddress, int count);

线圈方法

单线圈写        bool SetCoil(int iaddress, bool value);

多线圈写        bool SetCoil(int iaddress, bool[] value);

读多个线圈        List<bool> GetCoil(int iaddress, int count = 0);

读单个线圈        bool GetCoil(int iaddress);

离散寄存器方法

获取多个        List<bool> GetDiscreteInput(int iaddress, int count = 0);

获取单个        bool GetDiscreteInput(int iaddress);

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request