﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    public class TestModel
    {
        public TestModel()
        {
            MyProperty1 = new byte[8];
        }
        public byte[] MyProperty1 { get; set; }
        public bool MyProperty2 { get; set; }
        public bool MyProperty3 { get; set; }
        public bool MyProperty4 { get; set; }
        public bool MyProperty5 { get; set; }
        public ushort MyProperty6 { get; set; }
        public float My7 { get; set; }
        public float My8 { get; set; }
    }
}
